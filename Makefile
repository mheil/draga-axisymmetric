all:
	pdflatex paper_draft
	bibtex paper_draft
	bibtex paper_draft
	pdflatex paper_draft
	pdflatex paper_draft
	pdflatex paper_draft

diff: 
	latexdiff paper_submitted.tex paper_draft.tex > diff.tex
	pdflatex diff
	bibtex diff
	pdflatex diff
	pdflatex diff



dist:
	tar cvfz the_lot.tar.gz paper_draft.tex figures jfm.bst jfm.cls natbib.sty jfm-bibliography.bib Makefile
